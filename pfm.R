pfm <- function(sieges, resultats, tour = 2, paris = TRUE) {

  partis <- names(resultats)  ## liste des partis
  res <- rep(0, length(resultats)) ## initialisation des résultats

  if (tour == 1 & max(resultats) < 0.5*sum(resultats)) { ## au premier tour pas de répartition des sièges si pas de majorité abolue des voix pour une liste
    stop("Aucun parti n'a obtenu la majorité absolue au premier tour !")
  } else {

    votes <- sum(resultats)
    admis <- resultats > 0.05 * votes # seules les listes ayant obtenu 5 % des suffrages sont admises à la répartition des voix 

    maj <- ceiling(sieges / 2) # calcul de la prime majoritaire
    if(paris & sieges > 4) maj <- floor(sieges / 2) # règle pour Paris lorsqu'il y a plus de quatre sièges

    quotient <- sum(resultats[admis]) / (sieges - maj) # calcul du quotient électoral
    sieges <- sieges - maj # on enlève la prime majoritaire du nombre de sièges à distribuer à la proportionnelle 
    sieges_quotient <- floor(resultats[admis] / quotient) # distribution des sièges en fonction du quotient
    res[admis] <- res[admis] + sieges_quotient
    sieges <- sieges - sum(sieges_quotient) # on réactualise le nombre de sièges restant à distribuer

    while (sieges > 0) {
      # boucle dans laquelle on calcule la plus forte moyenne tant qu'il reste des sièges à attribuer
      moyenne <- resultats[admis] / (res[admis] + 1)
      if (sieges == 1 & sum(moyenne == max(moyenne)) > 1){
        # si, pour le dernier siège, plusieurs listes ont la même moyenne, on attribue le siège à la liste ayant obtenu le plus grand nombre de voix
        res[which.max(resultats[admis][moyenne == max(moyenne)])] <- res[which.max(resultats[admis][moyenne == max(moyenne)])] + 1
        break
      }
      res[admis][which.max(moyenne)] <- res[admis][which.max(moyenne)] + 1
      sieges <- sieges - 1
    }

    res[which.max(resultats)] <- res[which.max(resultats)] + maj  # on ne rajoute la prime majoritaire qu'à la fin pour ne pas fausser les calculs de la plus forte moyenne

  }
  names(res) <- partis
  return(res)
}