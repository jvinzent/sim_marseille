Simulateur des municipales à Marseille. Interface HTML UI de Shiny (R Studio http://www.rstudio.com/shiny/).
La version 1er tour est visible dans le commit du 2014-03-18
La version 2nd tour (ajout des noms des élus grâce aux listes) dans celui du 2014-04-03

# TODO
* Onglet conseil municipal avec demi-camembert
* Carte des secteurs (couleur selon liste majoritaire)
* Ajout dans les boutons de partage Twitter et Facebook du lien bit.ly généré dans l'onglet Partage